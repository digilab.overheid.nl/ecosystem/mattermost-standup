package main

import (
	"fmt"
	"io"
	"net/http"
	"strings"
	"sync"

	"embed"

	"github.com/mattermost/mattermost/server/public/model"
	"github.com/mattermost/mattermost/server/public/plugin"
	"github.com/mattermost/mattermost/server/public/pluginapi"
	"github.com/mattermost/mattermost/server/public/pluginapi/cluster"
)

//go:embed static/*
var assets embed.FS

// Plugin implements the interface expected by the Mattermost server to communicate between the server and plugin processes.
type Plugin struct {
	plugin.MattermostPlugin

	// configurationLock synchronizes access to the configuration.
	configurationLock sync.RWMutex

	// configuration is the active plugin configuration. Consult getConfiguration and
	// setConfiguration for usage.
	configuration *configuration

	client *pluginapi.Client

	// BotUserID
	botUserID string

	// backgroundJob is a job that executes periodically on only one plugin instance at a time
	backgroundJob *cluster.Job
}

// ServeHTTP handles HTTP requests. IMPROVE: use a router? See e.g. https://github.com/matterpoll/matterpoll/blob/master/server/plugin/api.go. But seems unnecessary with so few routes
func (p *Plugin) ServeHTTP(_ *plugin.Context, w http.ResponseWriter, req *http.Request) {
	// Read the request body
	body, err := io.ReadAll(req.Body)
	if err != nil {
		p.API.LogError("error reading request body", "error", err)
		http.Error(w, "error reading request body", http.StatusInternalServerError)
		return
	}

	defer req.Body.Close()

	// Validate the authenticity / authorization of requests sent to the integration
	if strings.HasPrefix(req.URL.Path, "/integration/") {
		if req.Header.Get("Mattermost-User-ID") == "" {
			http.Error(w, "Unauthorized", http.StatusUnauthorized)
			return
		}
	}

	switch req.URL.Path {
	case "/":
		fmt.Fprint(w, "Hello, world!") // TODO: replace with page containing information and instructions

	case "/integration/setup":
		switch req.Method {
		case http.MethodPost:
			if err := p.showSetup(body); err != nil {
				p.API.LogError("error showing setup", "error", err)
				http.Error(w, "error showing setup", http.StatusInternalServerError)
				return
			}
		default:
			html, err := assets.ReadFile("static/readme.html")
			if err != nil {
				p.API.LogError("error reading static file", "error", err)
				http.Error(w, "error reading static file", http.StatusInternalServerError)
				return
			}
			fmt.Fprint(w, string(html))
		}

	case "/integration/setup/store": // Note: different route from above, since both receive a POST request
		if err := p.storeSetup(body); err != nil {
			p.API.LogError("error storing setup", "error", err)
			http.Error(w, "error storing setup", http.StatusInternalServerError)
			return
		}

	case "/integration/response":
		if err := p.handleInitialResponse(body); err != nil {
			p.API.LogError("error handling initial user response", "error", err)
			http.Error(w, "error handling initial user response", http.StatusInternalServerError)
			return
		}

	case "/integration/response/store":
		if err := p.storeResponse(body); err != nil {
			p.API.LogError("error storing user response", "error", err)
			http.Error(w, "error storing user response", http.StatusInternalServerError)
			return
		}

	default:
		p.API.LogError("request received on unknown path", "path", req.URL.Path)
		http.NotFound(w, req)
	}

	w.WriteHeader(http.StatusOK)
}

func (p *Plugin) UserHasJoinedChannel(_ *plugin.Context, channelMember *model.ChannelMember, _ *model.User) {
	p.API.LogInfo("--> user has joined channel", "user", channelMember.UserId, "channel", channelMember.ChannelId)

	// If the added user was the bot, send a message with configure button in the channel
	if channelMember.UserId == p.botUserID {
		message := ":wave: Hi, thanks for adding me to this channel."
		err := p.sendBotMessage(channelMember.ChannelId, message, ConfigureStandupProps)
		if err != nil {
			return
		}
		return
	}

	// If the bot is not in the channel, do nothing
	channelMembers, appErr := p.API.GetChannelMembersByIds(channelMember.ChannelId, []string{p.botUserID})
	if appErr != nil {
		p.API.LogError("error getting channel members", "error", appErr)
		return
	}
	if len(channelMembers) == 0 {
		return
	}

	// If the added user is not the bot, send a configuration message
	if channelMember.UserId != p.botUserID {
		user, appErr := p.API.GetUser(channelMember.UserId)
		if appErr != nil {
			p.API.LogError("error getting user", "error", appErr)
			return
		}
		userName := user.GetDisplayName(model.ShowFullName)

		message := fmt.Sprintf(":wave: Hi @%s, welcome to the standup channel. Please configure your standup settings.", userName)
		err := p.sendBotMessage(channelMember.ChannelId, message, ConfigureStandupProps)

		if err != nil {
			return
		}
	}
}

// When a user leaves a channel and is currently part of the standup, remove the user from the standup and inform about this in the channel
func (p *Plugin) UserHasLeftChannel(_ *plugin.Context, channelMember *model.ChannelMember, _ *model.User) {
	// Get the current standup config
	config, err := p.getStandupConfig(channelMember.ChannelId)
	if err != nil || config.ChannelID == "" { // Note: getStandupConfig returns an empty config in case the standup config could not be found
		p.API.LogDebug("a user has left a channel, but cannot find a standup config for this channel, so ignoring", "error", err)
		return
	}

	// If the bot left the channel, remove the standup config
	if channelMember.UserId == p.botUserID {
		// Remove the standup config
		err = p.removeStandupConfig(channelMember.ChannelId)
		if err != nil {
			p.API.LogError("error removing standup config", "error", err)
			return
		}

		// Inform the channel
		message := "I have left the channel and removed my settings."
		err = p.sendBotMessage(channelMember.ChannelId, message, model.StringInterface{})
		if err != nil {
			return
		}

		return
	}

	// Remove the user from all standup days
	userIsRemovedFromConfig := false
	for i, userIDs := range config.WeekDayUserIDs {
		for j, userID := range userIDs {
			if userID == channelMember.UserId {
				config.WeekDayUserIDs[i] = append(userIDs[:j], userIDs[j+1:]...) // See https://github.com/golang/go/wiki/SliceTricks#delete (which also inclused less readable alternatives)
				userIsRemovedFromConfig = true
				break // Assume the user is only enabled once for this day
			}
		}
	}

	if userIsRemovedFromConfig {
		// Save the updated standup config
		if err := p.storeStandupConfig(config); err != nil {
			p.API.LogError("error storing standup config", "error", err)
			return
		}

		// Try to get the user display name (which may fail when the user has been completely removed from the team and/or server)
		user, _ := p.API.GetUser(channelMember.UserId)

		userName := "A user"
		if user != nil {
			userName = user.GetDisplayName(model.ShowFullName) // IMPROVE: use model.ShowNicknameFullName instead?
		}

		// Inform the channel
		message := fmt.Sprintf(
			"**%s** was removed from the standup because this user left the channel.",
			userName,
		)
		err := p.sendBotMessage(config.ChannelID, message, model.StringInterface{})
		if err != nil {
			return
		}
	}
}

// When a message is received, check if it is a command for the standup bot
func (p *Plugin) MessageHasBeenPosted(_ *plugin.Context, post *model.Post) {
	// Ignore messages sent by the bot itself
	if sentByPlugin, _ := post.GetProp(StandupBotMessageMarker).(bool); sentByPlugin {
		return
	}
	// Ignore system messages announcing the bot leaving the channel
	if post.Type == model.PostTypeRemoveFromChannel {
		return
	}

	// Get the channel the message was posted in
	channel, appErr := p.API.GetChannel(post.ChannelId)
	if appErr != nil {
		p.API.LogError("error getting channel", "error", appErr)
		return
	}

	// Check if it is a direct message
	handled, err := p.handleDirectMessage(channel, post)
	if err != nil {
		p.API.LogError("error handling direct message", "error", err)
	}
	if handled {
		return
	}

	// Check if the bot is mentioned
	_, err = p.handleMentionMessage(channel, post)
	if err != nil {
		p.API.LogError("error handling mention message", "error", err)
	}

	// Not intrested in this message
}

// Returns (handled, error):
// - handled is true if the message was handled by this function
// - error if an error occurred
func (p *Plugin) handleDirectMessage(channel *model.Channel, post *model.Post) (bool, error) {
	// Check if it is a private message
	if channel.Type != model.ChannelTypeDirect {
		// Not a direct message
		return false, nil
	}

	// Check if bot is member of the channel
	members, appErr := p.API.GetChannelMembersByIds(post.ChannelId, []string{p.botUserID})
	if appErr != nil {
		p.API.LogError("error getting channel members", "error", appErr)
		return false, appErr
	}
	if len(members) == 0 {
		// Bot is not a member of the channel
		return false, nil
	}

	// Send usage instructions
	message := "Hi, I'm the standup bot. I can help you run standups in your channels. Please add me to a channel and configure me there." +
		"\n" +
		"If I am not yet a member of your team, please ask your team lead to add me."

	err := p.sendBotMessage(post.ChannelId, message, model.StringInterface{})
	if err != nil {
		return true, err
	}

	return true, nil
}

// Returns (handled, error):
// - handled is true if the message was handled by this function
// - error if an error occurred
func (p *Plugin) handleMentionMessage(channel *model.Channel, post *model.Post) (bool, error) {
	// Check if the bot is mentioned
	if !strings.Contains(post.Message, "@standup-bot") {
		// Bot is not mentioned
		return false, nil
	}

	// Check if bot is a member of the team containing the channel
	team, appErr := p.API.GetTeam(channel.TeamId)
	if appErr != nil {
		p.API.LogError("error getting team", "error", appErr)
		return false, appErr
	}
	teamMember, appErr := p.API.GetTeamMember(team.Id, p.botUserID)
	if appErr != nil {
		p.API.LogError("error getting team member", "error", appErr)
		return false, appErr
	}
	if teamMember == nil {
		// Bot is not a member of the team
		return false, nil
	}

	message := "Hi, I'm the standup bot. I can help you run standups in this channel. Please configure me here."
	err := p.sendBotMessage(post.ChannelId, message, ConfigureStandupProps)
	if err != nil {
		return true, err
	}
	return true, nil
}

// See https://developers.mattermost.com/extend/plugins/server/reference/
