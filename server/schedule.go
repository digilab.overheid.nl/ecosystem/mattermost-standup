package main

import (
	"fmt"
	"strings"
	"time"

	"github.com/mattermost/mattermost/server/public/model"
	"github.com/mattermost/mattermost/server/public/pluginapi/cluster"
)

func (p *Plugin) OnActivate() error {
	// Start a ticker that runs every minute
	job, err := cluster.Schedule(
		p.API,
		"BackgroundJob",
		cluster.MakeWaitForRoundedInterval(time.Minute),
		p.BackgroundJob,
	)
	if err != nil {
		return fmt.Errorf("failed to schedule background job: %w", err)
	}
	p.backgroundJob = job

	return nil
}

// BackgroundJob is a job that runs on a schedule, started in OnActivate
func (p *Plugin) BackgroundJob() {
	// Define a map with the local current time for each location, used for lazy loading
	localTimes := make(map[string]cachedTime)

	// Loop over all configured standups
	keys, appErr := p.API.KVList(0, 200) // IMPROVE: add pagination in case more than 200 KV pairs are configured for the plugin on the server
	if appErr != nil {                   // Note: no error is returned if the key does not exist
		p.API.LogError("error loading existing plugin configs", "error", appErr.Message)
	}

	for _, key := range keys {
		// Ignore if not a config KV pair
		if !strings.HasPrefix(key, configPrefix) {
			continue
		}

		// Get the standup config
		config, err := p.getStandupConfig(key[len(configPrefix):])
		if err != nil {
			p.API.LogError("error getting standup config", "error", err)
			continue
		}

		if len(config.WeekDayUserIDs) < 7 {
			p.API.LogError("startup config for channel contains too few days, skipping", "channel", config.ChannelID)
			continue
		}

		// Get the local time from the map if available, otherwise set it
		localTime, found := localTimes[config.TimeZone]
		if !found {
			// Get the local time in the standup's time zone
			loc, err := time.LoadLocation(config.TimeZone)
			if err != nil {
				// Log the error and use UTC as default time zone, do not return
				p.API.LogError("error getting time zone", "locationName", config.TimeZone, "error", err)

				loc = time.UTC
			}

			t := time.Now().In(loc)

			localTime = cachedTime{
				Time:      t,
				TimeOfDay: t.Format(timeFormat),
			}

			// Map the weekday, since we use 0 = Monday, ... 6 = Sunday and time.Weekday() starts with 0 = Sunday
			if wd := t.Weekday(); wd == time.Sunday {
				localTime.Weekday = 6
			} else {
				localTime.Weekday = int(wd) - 1
			}

			localTimes[config.TimeZone] = localTime
		}

		// Only continue if the current time equals the start time or end time. IMPROVE: take into account that the server might have been down => keep a record of handled standups
		if localTime.TimeOfDay == config.StartTime { // IMPROVE: better solution than string comparison? Also below
			p.API.LogInfo("standup start time", "channel", config.ChannelID)

			// Standup start time: send all configured users a private message
			for _, userID := range config.WeekDayUserIDs[localTime.Weekday] { // Note: ensured above that this index exists
				// Create a channel for direct messages to the user. Note: this is how mattermost works. The channel has to be recreated in order to have permissions to post to it
				channel, appErr := p.API.GetDirectChannel(p.botUserID, userID)
				if appErr != nil {
					p.API.LogError("error creating direct channel", "error", appErr.Message)
					continue
				}

				message := "Can you post an update for the standup?"
				if config.Name != "" {
					message = fmt.Sprintf("Can you post an update for %s?", config.Name)
				}

				date := localTime.Time.Format("2006-01-02")

				props := model.StringInterface{
					"attachments": []*model.SlackAttachment{
						{
							Actions: []*model.PostAction{
								{
									Id:    "working",
									Name:  "Yes",
									Style: "success",
									Integration: &model.PostActionIntegration{
										URL: fmt.Sprintf("/plugins/%s/integration/response", "mattermost-standup"), // IMPROVE: get plugin slug from manifest
										Context: map[string]any{
											"channelId": config.ChannelID, // Note: the standup channel ID, not the direct channel ID
											"response":  "working",
											"date":      date,
										},
									},
								},
								{
									Id:    "previousresponse", // Note: the action ID must not contain underscores
									Name:  "Use previous response",
									Style: "default",
									Integration: &model.PostActionIntegration{
										URL: fmt.Sprintf("/plugins/%s/integration/response", "mattermost-standup"), // IMPROVE: get plugin slug from manifest
										Context: map[string]any{
											"channelId": config.ChannelID,
											"response":  "previous_response",
											"date":      date,
										},
									},
								},
								{
									Id:    "notworking", // Note: the action ID must not contain underscores
									Name:  "Not working today",
									Style: "danger",
									Integration: &model.PostActionIntegration{
										URL: fmt.Sprintf("/plugins/%s/integration/response", "mattermost-standup"), // IMPROVE: get plugin slug from manifest
										Context: map[string]any{
											"channelId": config.ChannelID,
											"response":  "not_working",
											"date":      date,
										},
									},
								},
							},
						},
					},
				}

				err := p.sendBotMessage(channel.Id, message, props)
				// Post a message in the channel that the config has been updated
				if err != nil {
					p.API.LogError("error sending message", "error", err)
					continue
				}
			}
		} else if localTime.TimeOfDay == config.EndTime {
			p.API.LogInfo("standup end time", "channel", config.ChannelID)

			// Get the latest entries for this standup
			entries, err := p.getLatestEntries(config.ChannelID)
			if err != nil {
				p.API.LogError("error getting latest entries", "error", err)
				return
			}

			// Get a list of all users that are enlisted for this day
			weekDayUserIDs := config.WeekDayUserIDs[localTime.Weekday]

			// In case no users are enlisted for this day, we are done
			if len(weekDayUserIDs) == 0 {
				return
			}

			// Construct a list of users that did not respond this day
			numResponses := 0
			var pendingUsers []string
			var usersNotWorking []string

			date := localTime.Time.Format("2006-01-02")
			for _, userID := range weekDayUserIDs {
				entry, found := entries[userID]
				if found && entry.Date == date {
					numResponses++

					if !entry.IsWorking {
						user, appErr := p.API.GetUser(userID)
						if appErr != nil {
							p.API.LogError("error getting user", "userID", userID, "error", appErr.Message)
							continue
						}

						usersNotWorking = append(usersNotWorking, user.GetDisplayName(model.ShowFullName)) // IMPROVE: use model.ShowNicknameFullName instead?
					}
				} else {
					user, appErr := p.API.GetUser(userID)
					if appErr != nil {
						p.API.LogError("error getting user", "userID", userID, "error", appErr.Message)
						continue
					}

					pendingUsers = append(pendingUsers, "@"+user.Username)
				}
			}

			// Send a message to the channel with a standup summary
			standupName := "the standup"
			if config.Name != "" {
				standupName = config.Name
			}

			pending := strings.Join(pendingUsers, ", ")
			if pending == "" {
				pending = "–" // Note: &endash; character, see comment in response.go. Also below
			}

			notWorking := strings.Join(usersNotWorking, ", ")
			if notWorking == "" {
				notWorking = "–"
			}

			// Post a summary in the channel
			message := fmt.Sprintf(
				"Summary for %s:\n\n**Number of responses:** %d\n\n**Pending members:**\n%s\n\n**Users not working today:**\n%s",
				standupName,
				numResponses,
				pending,
				notWorking,
			)
			err = p.sendBotMessage(config.ChannelID, message, model.StringInterface{})
			if err != nil {
				p.API.LogError("error sending message", "error", appErr.Message)
				return
			}

			// IMPROVE: in case a user updates or posts a response, update the summary?
		}
	}
}
