package main

import (
	"encoding/json"
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/mattermost/mattermost/server/public/model"
)

type standupConfig struct {
	ChannelID            string     `json:"channelId"` // Note: also stored in the KV store key, but added here for convenience
	TeamID               string     `json:"teamId"`    // Stored to be sure, currently unused
	Name                 string     `json:"name"`
	StartTime            string     `json:"startTime"` // IMPROVE: different type? Also below
	EndTime              string     `json:"endTime"`
	TimeZone             string     `json:"timeZone"`       // IMPROVE: different type?
	WeekDayUserIDs       [][]string `json:"weekDayUserIds"` // Note: outer index is day of week (0 = Monday etc.), inner slice is list of User IDs
	LastModifiedAt       time.Time  `json:"lastModifiedAt"`
	LastModifiedByUserID string     `json:"lastModifiedByUserID"`
}

const (
	configPrefix = "standup_config_"
	timeFormat   = "15:04"
)

// showSetup shows the dialog to configure a standup
func (p *Plugin) showSetup(body []byte) error {
	data := new(model.PostActionIntegrationRequest)
	if err := json.Unmarshal(body, data); err != nil {
		return fmt.Errorf("error unmarshaling PostActionIntegrationRequest: %w", err)
	}

	// Get the users that are in the channel
	users, appErr := p.API.GetUsersInChannel(data.ChannelId, "username", 0, 200) // Note: the maximum per page seems to be 200. IMPROVE: use pagination if more than 200?
	if appErr != nil {
		return fmt.Errorf("error opening getting users in channel: %s", appErr.Message)
	}

	// Validate that the user who sent the request still belongs to the channel
	userBelongsToChannel := false
	for _, user := range users {
		if user.Id == data.UserId { // Note: it seems safe to use data.UserId, see the comment in response.go
			userBelongsToChannel = true
			break
		}
	}
	if !userBelongsToChannel {
		return fmt.Errorf("ignoring setup request sent by user %s, which is no longer in channel %s", data.UserId, data.ChannelId)
	}

	// Get the current standup config
	config, err := p.getStandupConfig(data.ChannelId)
	if err != nil {
		p.API.LogInfo("could not get standup config", "channel", data.ChannelId, "error", err)
		// Note: only log the error, do not return
	}

	// Set the default config values in case unset
	if config.StartTime == "" {
		config.StartTime = "08:00"
	}
	if config.EndTime == "" {
		config.EndTime = "10:00"
	}
	if config.TimeZone == "" {
		config.TimeZone = "Europe/Amsterdam"
	}

	timezoneOpts := make([]*model.PostActionOptions, len(timezones))
	for i, tz := range timezones {
		timezoneOpts[i] = &model.PostActionOptions{
			Text:  tz,
			Value: tz,
		}
	}

	// Set the elements for the interactive dialog
	elements := []model.DialogElement{
		{
			DisplayName: "Standup name",
			Name:        "name",
			Type:        "text",
			Placeholder: "the standup",
			Optional:    true,
			Default:     config.Name,
		},
		{
			DisplayName: "Start time",
			Name:        "start_time",
			Type:        "text",
			MinLength:   4,
			MaxLength:   5,
			Default:     config.StartTime,
			HelpText:    "In 24 hour format, e.g. 08:00",
		},
		{
			DisplayName: "End time",
			Name:        "end_time",
			Type:        "text",
			Default:     config.EndTime,
			MinLength:   4,
			MaxLength:   5,
			HelpText:    "In 24 hour format, e.g. 10:00",
		},
		{
			DisplayName: "Time zone",
			Name:        "time_zone",
			Type:        "select",
			Options:     timezoneOpts,
			Default:     config.TimeZone,
		},
	}

	// Add the dialog elements for the user attendance
	for i, weekday := range []string{ // Note: not a numeric loop in combination with time.Weekday, since we start with Monday
		"Monday",
		"Tuesday",
		"Wednesday",
		"Thursday",
		"Friday",
		"Saturday",
		"Sunday",
	} {
		firstOfDay := true

		// Construct a map of the users that are already selected for this day, for quick lookup
		selectedUsers := make(map[string]struct{})
		if len(config.WeekDayUserIDs) > i {
			for _, userID := range config.WeekDayUserIDs[i] {
				selectedUsers[userID] = struct{}{}
			}
		}

		for _, user := range users {
			if user == nil || user.Id == p.botUserID { // Exclude the bot itself. Note: should never be nil, but checked to be sure to avoid panics
				continue
			}

			el := model.DialogElement{
				Name:        fmt.Sprintf("weekday_%d_%s", i, user.Id),
				Type:        "bool",
				Placeholder: user.GetDisplayName(model.ShowFullName), // IMPROVE: use model.ShowNicknameFullName instead?
				Optional:    true,
			}

			if _, found := selectedUsers[user.Id]; found {
				el.Default = "true"
			}

			if firstOfDay {
				el.DisplayName = fmt.Sprintf("Attendance on %s", weekday)
				firstOfDay = false
			}

			elements = append(elements, el)
			// Note: Mattermost does not allow checkbox groups yet, so we have to add an element for each checkbox. Leaving the DisplayName empty looks a bit strange. See https://mattermost.uservoice.com/forums/306457-general/suggestions/47341409-checkbox-group-as-an-element-for-interactive-dialo
		}
	}

	// Show an interactive dialog using the TriggerId from the request body
	if appErr = p.API.OpenInteractiveDialog(model.OpenDialogRequest{
		TriggerId: data.TriggerId,
		URL:       fmt.Sprintf("/plugins/%s/integration/setup/store", "mattermost-standup"), // The URL the form data is posted to when submitted. IMPROVE: get plugin slug from manifest
		Dialog: model.Dialog{
			Title:            "Configure standup",
			IntroductionText: "Please configure the standup.",
			Elements:         elements,
			SubmitLabel:      "Save",
			// IMPROVE: set a CallbackId? Seems unnecessary
		},
	}); appErr != nil {
		return fmt.Errorf("error opening interactive dialog: %s", appErr.Message)
	}

	return nil
}

// storeSetup handles the post from the dialog to configure a standup
func (p *Plugin) storeSetup(body []byte) error {
	data := new(model.SubmitDialogRequest)
	if err := json.Unmarshal(body, data); err != nil {
		return fmt.Errorf("error unmarshaling SubmitDialogRequest: %w", err)
	}

	// Validate that the user who sent the request still belongs to the channel
	if channelMembers, appErr := p.API.GetChannelMembersByIds(data.ChannelId, []string{data.UserId}); appErr != nil {
		return fmt.Errorf("error validating that the user belongs to the channel: %s", appErr.Message)
	} else if len(channelMembers) == 0 {
		return fmt.Errorf("ignoring setup request sent by user %s, which is no longer in channel %s", data.UserId, data.ChannelId)
	}

	// Validate the start and end times and ensure that start time < end time
	var startTime, endTime time.Time
	var err error
	st, _ := data.Submission["start_time"].(string)
	if startTime, err = time.Parse(timeFormat, strings.TrimSpace(st)); err != nil {
		if post := p.API.SendEphemeralPost(data.UserId, &model.Post{
			UserId:    p.botUserID,
			ChannelId: data.ChannelId,
			Message:   fmt.Sprintf("Could not save the standup configuration because the start time '%s' was unrecognized.", st),
		}); post.Id == "" {
			return fmt.Errorf("error sending ephemeral post to user %s", data.UserId)
		}

		return nil
	}

	et, _ := data.Submission["end_time"].(string)
	if endTime, err = time.Parse(timeFormat, strings.TrimSpace(et)); err != nil {
		if post := p.API.SendEphemeralPost(data.UserId, &model.Post{
			UserId:    p.botUserID,
			ChannelId: data.ChannelId,
			Message:   fmt.Sprintf("Could not save the standup configuration because the end time '%s' was unrecognized.", et),
		}); post.Id == "" {
			return fmt.Errorf("error sending ephemeral post to user %s", data.UserId)
		}

		return nil
	}

	if !startTime.Before(endTime) {
		if post := p.API.SendEphemeralPost(data.UserId, &model.Post{
			UserId:    p.botUserID,
			ChannelId: data.ChannelId,
			Message:   "Could not save the standup configuration. The start time should be earlier than the end time.",
		}); post.Id == "" {
			return fmt.Errorf("error sending ephemeral post to user %s", data.UserId)
		}

		return nil
	}

	// Store the config
	config := standupConfig{
		ChannelID:            data.ChannelId,
		TeamID:               data.TeamId,
		StartTime:            startTime.Format(timeFormat), // Re-format, since e.g. the formatter accepts "8:30", but we need "08:30". Also below
		EndTime:              endTime.Format(timeFormat),
		LastModifiedAt:       time.Now(),
		LastModifiedByUserID: data.UserId,
		WeekDayUserIDs:       make([][]string, 7),
	}
	config.Name, _ = data.Submission["name"].(string)
	config.TimeZone, _ = data.Submission["time_zone"].(string)

	for key, value := range data.Submission {
		if strings.HasPrefix(key, "weekday_") && value == true && len(key) > 10 { // Note: only consider the `true`` values
			weekday, _ := strconv.Atoi(key[8:9])
			userID := key[10:]

			config.WeekDayUserIDs[weekday] = append(config.WeekDayUserIDs[weekday], userID)
		}
	}

	if err = p.storeStandupConfig(config); err != nil {
		return fmt.Errorf("error storing standup config: %w", err)
	}

	user, appErr := p.API.GetUser(config.LastModifiedByUserID)
	if appErr != nil {
		p.API.LogError("error getting user", "userID", config.LastModifiedByUserID, "error", appErr.Message)
		// Note: only log the error, do not return
	}

	message := "The standup config was updated."
	if user != nil {
		message = fmt.Sprintf("The standup config was updated by **%s**.", user.GetDisplayName(model.ShowFullName)) // IMPROVE: use model.ShowNicknameFullName instead?
	}

	// Have the bot join the channel if it is not already a member
	if _, appErr = p.API.GetChannelMember(config.ChannelID, p.botUserID); appErr != nil {
		if _, appErr := p.API.AddChannelMember(config.ChannelID, p.botUserID); appErr != nil {
			return fmt.Errorf("error adding bot to channel: %s", appErr.Message)
		}
	}

	// Post a message in the channel that the config has been updated
	err = p.sendBotMessage(config.ChannelID, message, model.StringInterface{})
	if err != nil {
		return fmt.Errorf("error sending message: %s", appErr.Message)
	}

	return nil
}

func (p *Plugin) getStandupConfig(channelID string) (config standupConfig, err error) {
	configJSON, appErr := p.API.KVGet(configPrefix + channelID)
	if appErr != nil { // Note: no error is returned if the key does not exist
		err = fmt.Errorf("error loading existing config: %s", appErr.Message)
		return
	}

	// Decode the config
	if len(configJSON) != 0 {
		if err = json.Unmarshal(configJSON, &config); err != nil {
			err = fmt.Errorf("error unmarshaling standup config: %w", err)
			return
		}
	}

	return
}
func (p *Plugin) removeStandupConfig(channelID string) (err error) {
	if appErr := p.API.KVDelete(configPrefix + channelID); appErr != nil {
		err = fmt.Errorf("error deleting standup config: %s", appErr.Message)
		return
	}

	return
}

func (p *Plugin) storeStandupConfig(config standupConfig) error {
	configJSON, err := json.Marshal(config)
	if err != nil {
		return fmt.Errorf("error marshaling standup config: %w", err)
	}

	if appErr := p.API.KVSet(configPrefix+config.ChannelID, configJSON); appErr != nil {
		return fmt.Errorf("error storing standup config: %s", appErr.Message)
	}

	return nil
}
