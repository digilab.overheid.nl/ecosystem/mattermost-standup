package main

import (
	"encoding/gob"
	"fmt"
	"reflect"
	"time"

	"github.com/mattermost/mattermost/server/public/model"
	"github.com/mattermost/mattermost/server/public/pluginapi"
)

// configuration captures the plugin's external configuration as exposed in the Mattermost server
// configuration, as well as values computed from the configuration. Any public fields will be
// deserialized from the Mattermost server configuration in OnConfigurationChange.
//
// As plugins are inherently concurrent (hooks being called asynchronously), and the plugin
// configuration can change at any time, access to the configuration must be synchronized. The
// strategy used in this plugin is to guard a pointer to the configuration, and clone the entire
// struct whenever it changes. You may replace this with whatever strategy you choose.
//
// If you add non-reference types to your configuration struct, be sure to rewrite Clone as a deep
// copy appropriate for your types.
type configuration struct {
}

type cachedTime struct {
	Time      time.Time // Local time
	Weekday   int       // Starting with 0 = Monday
	TimeOfDay string    // In 24 hour format, e.g. 12:34
}

func init() {
	gob.Register([]map[string]any{}) // Required for post.SetProps
	gob.Register(map[string]any{})   // Required for post.SetProps
}

// Clone shallow copies the configuration. Your implementation may require a deep copy if
// your configuration has reference types.
func (c *configuration) Clone() *configuration {
	var clone = *c
	return &clone
}

// getConfiguration retrieves the active configuration under lock, making it safe to use
// concurrently. The active configuration may change underneath the client of this method, but
// the struct returned by this API call is considered immutable.
func (p *Plugin) getConfiguration() *configuration {
	p.configurationLock.RLock()
	defer p.configurationLock.RUnlock()

	if p.configuration == nil {
		return &configuration{}
	}

	return p.configuration
}

// setConfiguration replaces the active configuration under lock.
//
// Do not call setConfiguration while holding the configurationLock, as sync.Mutex is not
// reentrant. In particular, avoid using the plugin API entirely, as this may in turn trigger a
// hook back into the plugin. If that hook attempts to acquire this lock, a deadlock may occur.
//
// This method panics if setConfiguration is called with the existing configuration. This almost
// certainly means that the configuration was modified without being cloned and may result in
// an unsafe access.
func (p *Plugin) setConfiguration(configuration *configuration) {
	p.configurationLock.Lock()
	defer p.configurationLock.Unlock()

	if configuration != nil && p.configuration == configuration {
		// Ignore assignment if the configuration struct is empty. Go will optimize the
		// allocation for same to point at the same memory address, breaking the check
		// above.
		if reflect.ValueOf(*configuration).NumField() == 0 {
			return
		}

		panic("setConfiguration called with the existing configuration")
	}

	p.configuration = configuration
}

// OnConfigurationChange is invoked when configuration changes may have been made.
func (p *Plugin) OnConfigurationChange() error {
	if p.client == nil {
		p.client = pluginapi.NewClient(p.API, p.Driver)
	}

	var configuration = new(configuration)

	// Load the public configuration fields from the Mattermost server configuration.
	if err := p.API.LoadPluginConfiguration(configuration); err != nil {
		return fmt.Errorf("failed to load plugin configuration: %w", err)
	}

	// Ensure that the bot user exists
	// botID, err := p.API.EnsureBotUser(&model.Bot{
	// 	Username:    "standup-bot",
	// 	DisplayName: "Standup bot",
	// 	Description: "A bot account that helps users to submit entries for a standup.",
	// })
	// if err != nil {
	// 	return fmt.Errorf("error ensuring bot user: %w", err)
	// }
	botID, err := p.client.Bot.EnsureBot(&model.Bot{
		Username:    "standup-bot",
		DisplayName: "Standup bot",
		Description: "A bot account that helps users to submit entries for a standup.",
	}, pluginapi.ProfileImagePath("/assets/logo.png"))
	if err != nil {
		return fmt.Errorf("error ensuring bot user: %w", err)
	}

	// Store the bot user ID directly in the plugin struct and not in the configuration, see e.g. https://github.com/mattermost/mattermost-plugin-demo/blob/master/server/configuration.go#L224
	p.botUserID = botID

	p.setConfiguration(configuration) // Note: apparently does not fire OnConfigurationChange, otherwise an infinite loop would occur

	return nil
}
