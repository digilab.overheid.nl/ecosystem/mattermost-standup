package main

import (
	"errors"
	"fmt"

	"github.com/mattermost/mattermost/server/public/model"
)

// Send a message as the specified user to the specified channel
func (p *Plugin) sendUserMessage(userID string, channelID string, message string, props model.StringInterface) (*model.Post, error) {
	newPost := &model.Post{
		UserId:    userID,
		ChannelId: channelID,
		Message:   message,
	}

	props[StandupBotMessageMarker] = true

	newPost.SetProps(props)

	post, err := p.API.CreatePost(newPost)
	if err != nil {
		p.API.LogError("error sending message", "error", err.Message)
		return nil, err
	}

	return post, nil
}

func (p *Plugin) sendBotMessage(channelID string, message string, props model.StringInterface) error {
	_, err := p.sendUserMessage(p.botUserID, channelID, message, props)
	return err
}

func (p *Plugin) updateBotMessage(existingPostID string, message string, props model.StringInterface) error {
	post := &model.Post{
		Id: existingPostID,
		// Note: no channel ID required for updating posts
		Message: message,
	}

	props[StandupBotMessageMarker] = true

	post.SetProps(props)

	if _, appErr := p.API.UpdatePost(post); appErr != nil {
		return errors.New(appErr.Message)
	}

	return nil
}

var StandupBotMessageMarker = "standupBotMessage"
var ConfigureStandupProps = model.StringInterface{
	"attachments": []*model.SlackAttachment{
		{
			Actions: []*model.PostAction{
				{
					Id:    "setup",
					Name:  "Configure the standup settings for this channel",
					Style: "primary",
					Integration: &model.PostActionIntegration{
						URL: fmt.Sprintf("/plugins/%s/integration/setup", "mattermost-standup"), // IMPROVE: get plugin slug from manifest
					},
				},
			},
		},
	},
}
