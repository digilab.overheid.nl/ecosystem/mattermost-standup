package main

import (
	"encoding/json"
	"fmt"
	"strings"

	"github.com/mattermost/mattermost/server/public/model"
)

// latestEntry defines an entry submitted for the latest standup. Used for 'Use previous response', editing responses, and for posting reports on standup end times
type latestEntry struct {
	Date      string `json:"date"` // In 2006-01-02 format, in the local time of the standup. Note: suitable for string comparison/ordering
	IsWorking bool   `json:"isWorking"`
	PostID    string `json:"postId"` // ID of the post in which the user entry was posted in the standup channel
	Done      string `json:"done"`
	Doing     string `json:"doing"`
	Blockers  string `json:"blockers"`
}

// entryState is used to pass data in the state of an interactive dialog
type entryState struct {
	Date      string `json:"date"`
	ChannelID string `json:"channelId"`
	PostID    string `json:"postId"` // ID of the post in which the user was asked to submit an entry. Used to update this post after submission
}

const latestEntriesPrefix = "latest_entries_"

// handleInitialResponse handles the initial response of the user on a request to submit an update for a standup
func (p *Plugin) handleInitialResponse(body []byte) error {
	data := new(model.PostActionIntegrationRequest)
	if err := json.Unmarshal(body, data); err != nil {
		return fmt.Errorf("error unmarshaling PostActionIntegrationRequest: %w", err)
	}

	// Get the user
	user, appErr := p.API.GetUser(data.UserId) // Note: it seems safe to use data.UserId, Mattermost returns a 401 Unauthorized response when trying to post data with another user ID that is inconsistent with the request's auth cookie
	if appErr != nil {
		return fmt.Errorf("error getting user: %s", appErr.Message)
	}

	// Inform the standup channel that the user has responded
	channelID, _ := data.Context["channelId"].(string) // Note: the standup channel ID, not the direct channel ID
	response, _ := data.Context["response"].(string)
	date, _ := data.Context["date"].(string)

	if channelID == "" || response == "" || date == "" {
		return fmt.Errorf("empty channel ID (%s) or response (%s) or date (%s), cannot continue", channelID, response, date)
	}

	// Set the state
	stateJSON, err := json.Marshal(entryState{
		Date:      date,
		ChannelID: channelID,
		PostID:    data.PostId,
	})
	if err != nil {
		return fmt.Errorf("error marshaling state: %w", err)
	}

	// Get the latest entries for this standup
	entries, err := p.getLatestEntries(channelID)
	if err != nil {
		return fmt.Errorf("error getting latest entries: %w", err)
	}

	previousUserEntry := entries[data.UserId]

	// If a newer standup entry has been stored, update the previous post that the entry can no longer be edited
	if previousUserEntry.Date > date {
		message := "This submission can no longer be edited, since it is too old. You can only edit your latest response."

		err = p.updateBotMessage(data.PostId, message, model.StringInterface{})
		if err != nil {
			return fmt.Errorf("error updating message: %s", err)
		}

		return nil
	}

	switch response {
	case "not_working":
		// If a post already exists for this day, update it. Otherwise, create a new post
		message := fmt.Sprintf("**%s** reported not working today", user.GetDisplayName(model.ShowFullName)) // IMPROVE: use model.ShowNicknameFullName instead?

		latestPostID := previousUserEntry.PostID

		if previousUserEntry.Date == date {
			err = p.updateBotMessage(latestPostID, message, model.StringInterface{})
			if err != nil {
				return fmt.Errorf("error updating message: %s", err)
			}
		} else {
			// Post on behalf of the user
			post, err := p.sendUserMessage(data.UserId, channelID, message, model.StringInterface{})
			if err != nil {
				return fmt.Errorf("error sending message: %s", err)
			}
			latestPostID = post.Id
		}

		// Update the latest user entry, copying the values for done/doing/blockers
		entries[data.UserId] = latestEntry{
			Date:      date,
			IsWorking: false,
			PostID:    latestPostID,
			Done:      previousUserEntry.Done,
			Doing:     previousUserEntry.Doing,
			Blockers:  previousUserEntry.Blockers,
		}

		if err = p.storeLatestEntries(channelID, entries); err != nil {
			return fmt.Errorf("error storing latest entries: %w", err)
		}

		if err = p.sendFeedbackPost(data.PostId, channelID, date); err != nil {
			return fmt.Errorf("error sending feedback post: %w", err)
		}

	case "working", "previous_response":
		// Show an interactive dialog asking the user for input

		// In case of an edit of the current submission or 'Use previous response': prefill the fields
		var done, doing, blockers string
		if previousUserEntry.Date == date || response == "previous_response" {
			done = previousUserEntry.Done
			doing = previousUserEntry.Doing
			blockers = previousUserEntry.Blockers
		}

		if appErr = p.API.OpenInteractiveDialog(model.OpenDialogRequest{
			TriggerId: data.TriggerId,
			URL:       fmt.Sprintf("/plugins/%s/integration/response/store", "mattermost-standup"), // The URL the form data is posted to when submitted. IMPROVE: get plugin slug from manifest
			Dialog: model.Dialog{
				Title:            "Standup submission",
				IntroductionText: "Please submit your input.", // IMPROVE: include the day and name of the standup
				Elements: []model.DialogElement{
					{
						DisplayName: "What have you done?",
						Name:        "done",
						Type:        "textarea",
						Optional:    true,
						Default:     done,
					},
					{
						DisplayName: "What are you going to do?",
						Name:        "doing",
						Type:        "textarea",
						Optional:    true,
						Default:     doing,
					},
					{
						DisplayName: "Do you have any blockers?",
						Name:        "blockers",
						Type:        "textarea",
						Optional:    true,
						Default:     blockers,
					},
				},
				SubmitLabel: "Submit",
				State:       string(stateJSON),
				// IMPROVE: set a CallbackId? Seems unnecessary
			},
		}); appErr != nil {
			return fmt.Errorf("error opening interactive dialog: %s", appErr.Message)
		}

	default:
		return fmt.Errorf("received unknown response: %s", response)
	}

	return nil
}

// storeResponse handles the response of the user when submitting the dialog to provide an update for a standup
func (p *Plugin) storeResponse(body []byte) error {
	data := new(model.SubmitDialogRequest)
	if err := json.Unmarshal(body, data); err != nil {
		return fmt.Errorf("error unmarshaling SubmitDialogRequest: %w", err)
	}

	// Validate that the user who sent the request still belongs to the channel
	if channelMembers, appErr := p.API.GetChannelMembersByIds(data.ChannelId, []string{data.UserId}); appErr != nil {
		return fmt.Errorf("error validating that the user belongs to the channel: %s", appErr.Message)
	} else if len(channelMembers) == 0 {
		return fmt.Errorf("ignoring setup request sent by user %s, which is no longer in channel %s", data.UserId, data.ChannelId)
	}

	// Get the user
	user, appErr := p.API.GetUser(data.UserId)
	if appErr != nil {
		return fmt.Errorf("error getting user: %s", appErr.Message)
	}

	// Decode the state from the request
	var state entryState
	if err := json.Unmarshal([]byte(data.State), &state); err != nil {
		return fmt.Errorf("error unmarshaling PostActionIntegrationRequest: %w", err)
	}

	// Get the current standup config
	config, err := p.getStandupConfig(state.ChannelID)
	if err != nil {
		return fmt.Errorf("error getting standup config: %w", err)
	}

	standupName := "the standup"
	if config.Name != "" {
		standupName = config.Name
	}

	done, _ := data.Submission["done"].(string)
	doing, _ := data.Submission["doing"].(string)
	blockers, _ := data.Submission["blockers"].(string)

	// Get the latest entries for this standup
	entries, err := p.getLatestEntries(state.ChannelID)
	if err != nil {
		p.API.LogError("error getting latest entries", "error", err)
		// Note: only log the error, do not return
	}

	message := fmt.Sprintf(
		"**%s** submitted an update for %s.\n\n**Done:**\n%s\n\n**Doing:**\n%s\n\n**Blockers:**\n%s",
		// IMPROVE: get the standup name (default: 'the standup') from the standup config
		user.GetDisplayName(model.ShowFullName),
		standupName,
		addBullets(done),
		addBullets(doing),
		addBullets(blockers),
	)

	latestUserEntry := entries[user.Id]
	latestPostID := latestUserEntry.PostID

	switch {
	case state.Date < latestUserEntry.Date:
		{
			// If a newer standup entry has been stored, this entry can no longer be edited. Note: this should be prevented above, but checked here to be sure in case the user has multiple open dialogs, e.g. using multiple tabs or devices
			message := "This submission can no longer be edited, since it is too old. You can only edit your latest response."
			err = p.updateBotMessage(state.PostID, message, model.StringInterface{})
			if err != nil {
				return fmt.Errorf("error updating message: %s", err)
			}

			p.API.LogInfo("ignoring standup entry since a this is an update attempt of an older submission")
			return nil
		}
	case state.Date == latestUserEntry.Date: // Note: string comparison, which works fine for dates in (partial) RFC3339 format
		{
			// If the user has already submitted an entry for the specific day, update it
			err = p.updateBotMessage(latestUserEntry.PostID, message, model.StringInterface{})

			if err != nil {
				return fmt.Errorf("error updating message: %s", err)
			}
		}
	default:
		{
			// If the entry is later than the previously saved entry, post a new message in the standup channel
			// Post on behalf of the user
			post, err := p.sendUserMessage(data.UserId, state.ChannelID, message, model.StringInterface{})
			if err != nil {
				return fmt.Errorf("error sending message: %s", err)
			}
			latestPostID = post.Id
		}
	}

	// Update the existing message by "Thanks for your response" and a button "Edit your response". Note: also done when the user has already submitted an entry for today, since the standup may have been rescheduled during the day
	if err = p.sendFeedbackPost(state.PostID, state.ChannelID, state.Date); err != nil {
		return fmt.Errorf("error sending feedback post: %w", err)
	}

	// Update and store the entries
	if entries == nil {
		entries = make(map[string]latestEntry)
	}

	entries[user.Id] = latestEntry{
		Date:      state.Date,
		IsWorking: true,
		PostID:    latestPostID,
		Done:      done,
		Doing:     doing,
		Blockers:  blockers,
	}

	if err = p.storeLatestEntries(state.ChannelID, entries); err != nil {
		return fmt.Errorf("error storing latest entries: %w", err)
	}

	return nil
}

// getLatestEntries returns a map with user IDs as keys and latest entries as values for the specified channel ID
func (p *Plugin) getLatestEntries(channelID string) (entries map[string]latestEntry, err error) {
	entriesJSON, appErr := p.API.KVGet(latestEntriesPrefix + channelID)
	if appErr != nil { // Note: no error is returned if the key does not exist
		err = fmt.Errorf("error loading latest entries: %s", appErr.Message)
		return
	}

	// Decode the latest entries
	if len(entriesJSON) != 0 {
		if err = json.Unmarshal(entriesJSON, &entries); err != nil {
			err = fmt.Errorf("error unmarshaling latest entries: %w", err)
			return
		}
	}

	return
}

func (p *Plugin) storeLatestEntries(channelID string, entries map[string]latestEntry) error {
	entriesJSON, err := json.Marshal(entries)
	if err != nil {
		return fmt.Errorf("error marshaling standup config: %w", err)
	}

	if appErr := p.API.KVSet(latestEntriesPrefix+channelID, entriesJSON); appErr != nil {
		return fmt.Errorf("error storing entries in storage: %s", appErr.Message)
	}

	return nil
}

// addBullets ensures that each line starts with a bullet
func addBullets(s string) string {
	if s == "" {
		return "–" // Indicator for none/empty. Note: this is an &endash; character, since using a normal dash (-) formats the line above as header on Mattermost mobile
	}

	lines := strings.Split(s, "\n")
	for i, line := range lines {
		switch strings.TrimSpace(line) {
		// If the line is empty, or contains only an endash, or emdash, do not prefix it
		case "", "–", "—":
			// Nothing to be done

		// In case the line is a normal dash, replace it with an endash without prefix
		case "-":
			lines[i] = "–"

		// Otherwise, prefix the line with a dash (bullet) if it doesn't have this prefix yet
		default:
			if !strings.HasPrefix(line, "- ") {
				lines[i] = "- " + line
			}
		}
	}

	return strings.Join(lines, "\n")
}

func (p *Plugin) sendFeedbackPost(existingPostID string, channelID string, date string) (err error) {
	props := model.StringInterface{
		"attachments": []*model.SlackAttachment{
			{
				Actions: []*model.PostAction{
					{
						Id:    "edit",
						Name:  "Edit your response",
						Style: "primary",
						Integration: &model.PostActionIntegration{
							URL: fmt.Sprintf("/plugins/%s/integration/response", "mattermost-standup"), // IMPROVE: get plugin slug from manifest
							Context: map[string]any{
								"channelId": channelID, // Note: the standup channel ID, not the direct channel ID
								"response":  "working", // IMPROVE: add an option to report as not working?
								"date":      date,
							},
						},
					},
				},
			},
		},
	}
	message := "Thanks for your response."
	err = p.updateBotMessage(existingPostID, message, props)

	return err
}
