# Mattermost Standup plugin

## Installation

1. Clone the repository to your local machine.
2. Navigate to the project directory in your terminal.
3. Run `make dist` to build the plugin. This will generate a .tar.gz file.
4. Log in to your Mattermost server.
5. Go to **System Console > Plugin Management**.
6. Ensure that your Mattermost configuration allows plugin uploads.
7. Click **Upload Plugin** and select the .tar.gz file you generated earlier.
8. After the upload completes, click **Activate** to start using the Standup plugin.

## Usage
1. Add the Standup bot to your team where you want to use the Standup bot.
2. Mention @standup-bot in or invite the Standup bot to any channel where you want it to post standup updates.
3. Once the Standup bot is in the channel, it will prompt you to configure it for that channel. Follow the instructions provided by the bot to complete the configuration.
4. After configuration, the bot will automatically post standup updates in the channel at the configured times.

## Removal
1. \\remove @standup-bot from your channel
2. @standup-bot will remove its configuration

